function bookSearch(){
    var search = document.getElementById('search').value
    document.getElementById('results').innerHTML = ""
    console.log(search)

    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
        dataType: "jsonp",
        
        success: function(data) {
            for(i = 0; i < data.items.length; i++){
                results.innerHTML +=
                "<tr>" +
                    "<td>" +
                        "<img src=" + data.items[i].volumeInfo.imageLinks.thumbnail + "></img>" +
                    "<td>" + data.items[i].volumeInfo.title + "</td>" +
                    "<td>" + data.items[i].volumeInfo.authors[0] + "</td>" +
                    "<td>" + data.items[i].volumeInfo.publishedDate + "</td>" +
                    "<td>" + data.items[i].volumeInfo.description + "</td>" +
                    "<td>" + '<button id="button" type="button">Click Me</button>' + "</td>" +
                "</tr>"
            }
        },

        type : 'GET'
    });
}
document.getElementById('button').addEventListener('click',bookSearch, false)