from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from app1.views import index
from selenium import webdriver
import time
import unittest

# Create your tests here.

class StoryUnitTest(TestCase):
    def test_story_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_story_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'base.html')

class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def checkAppFunctionality(self):
        selenium = self.selenium
        selenium.get('http://localhost:8000/')
        self.assertIn('Story 9', selenium.title)

        selenium.implicitly_wait(5)

        search = selenium.find_element_by_id('search')
        search.send_key('harry potter')
        button = selenium.find_element_by_id('button')
        button.click()

        selenium.implicitly_wait(10)

        self.assertIn('JK Rowling', selenium.page_source)


    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()
